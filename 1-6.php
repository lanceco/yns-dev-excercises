<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-6</title>
	</head>
	<body>
		<table align="center">
		    <tr>
		        <td>First name: </td>
		        <td><?php echo $_POST["fName"]; ?></td>
		    </tr>

		    <tr>
		        <td>Last name:</td>
		        <td><?php echo $_POST["lName"]; ?></td>
		    </tr>

			<tr>
		        <td>Email address:</td>
		        <td><?php echo $_POST["email"]; ?></td>
		    </tr>

		    <tr>
		        <td>Age</td>
		        <td><?php echo $_POST["age"]; ?></td>
		    </tr>

		    <tr>
		        <td>Gender:</td>
		        <td><?php echo $_POST["gender"]; ?></td>
		    </tr>		
		</table>
	</body>
</html>

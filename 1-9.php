<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-9</title>
	</head>
	<body>
		<table align="center">
		    <tr>
		        <td>First name</td>
		        <td>Last name</td>
		        <td>Email</td>
		        <td>Age</td>
	        	<td>Gender</td>
		    </tr>
	        <?php
			    $fp = fopen("person.csv", "r");
			    while (($line = fgetcsv($fp)) !== FALSE) {
			        //generate HTML
			        echo '<tr>';
			        foreach ($line as $cell) {
			            echo '<td>';
			            echo htmlentities($cell);
			            echo '</td>';
			        }
			        echo '</tr>';
			    }
			    fclose($fp);
			?>
		</table>
	</body>
</html>

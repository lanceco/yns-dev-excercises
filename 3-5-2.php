<!DOCTYPE html>
<?php 
	session_start();
	if(!isset($_SESSION['username'])) {
		header('location:3-5.php');
	}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>3-5-2</title>
		<script>
			function myFunction(x) {
				if (x == 'records') window.location.href = "3-5-4.php";
				if (x == 'logout') window.location.href = "3-5-5.php";
			}
		</script>
	</head>
	<body>
		<form action="3-5-3.php" method="post" enctype="multipart/form-data">
			<table align="center">
			    <tr>
			        <td>Enter first name: </td>
			        <td><input type="text" name="fName"></td>
			    </tr>

			    <tr>
			        <td>Enter last name:</td>
			        <td><input type="text" name="lName"></td>
			    </tr>

				<tr>
			        <td>Enter email address:</td>
			        <td><input type="text" name="email"></td>
			    </tr>

			    <tr>
			        <td>Enter age:</td>
			        <td><input type="text" name="age"></td>
			    </tr>

			    <tr>
			        <td>Enter gender:</td>
			        <td><input type="radio" name="gender" value="Male" checked> Male</td>
  					<td><input type="radio" name="gender" value="Female"> Female</td>
			    </tr>		

			    <tr>
			        <td>Select image to upload:</td>
			        <td><input type="file" name="fileToUpload" id="fileToUpload"></td>
			    </tr>		    

			    <tr>
			        <td></td>
			        <td><input type="submit" name="submit" value="                Submit                "></td>
			    </tr>
			</table>
		</form>
		<table align="center">
			<tr>	
		        <td></td>
		        <td><button onclick="myFunction('records')">Records</button></td>
		    </tr>

		    <tr>
		        <td></td>
		        <td><button onclick="myFunction('logout')">Log out</button></td>
		    </tr>
		</table>
	</body>
</html>

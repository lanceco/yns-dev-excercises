<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>5-1</title>
		<script>
			function redirect() {
				window.location.href = "5-1.php";
			}
		</script>
	</head>
	<body>
		<fieldset>
			<legend align="center">Quiz</legend>
			<?php
				$scoreCount = 0;
				$hasError = false;
				if (isset($_POST['submit'])):
					$servername = "localhost";
					$username = "root";
					$password = "";
					$database = "test";
		        	//Create connection
					$conn = new mysqli($servername, $username, $password, $database);
					//Check connection
					if ($conn->connect_error) die("Connection Failed: ". $conn->connect_error);
					// echo "Connection Successful <br>";

					$sql = "SELECT id, question, answer, choice1, choice2 FROM quiz";
					$result = $conn->query($sql);
					
					for ($k = 1; $k <= 10; $k++):
					    if (!empty($_POST['num' . $k])):
					    	$selected[] = $_POST['num' . $k];
				    	else:
					    	$hasError = true;
					    endif;
					endfor;
					
					if ($hasError == false):
						$i = 0;
						$j = 1;
						if ($result->num_rows > 0):
						    // output data of each row
						    while($row = $result->fetch_assoc()):
				    			echo "<p>". $j .") ". $row["question"] ."</p>";
				    			if ($selected[$i] === $row["answer"]):
				    				$scoreCount++;
				    				echo "<p><font color='green'>". $selected[$i] ."</font></p>";
			    				else:
			    					echo "<p><font color='red'>". $selected[$i] ."</font></p>";
				    			endif;
							  	$i++;
							  	$j++;
				        	endwhile;
						endif;
					endif;
					$conn->close();
				endif;
			?>
		</fieldset>
		<?php
			if ($hasError == false): 
				echo "<p>Your Score is: ". $scoreCount ."</p>";
			else:
				echo "<p><font color='red'>All questions must be answered</font></p>";
			endif;
		?>
		<button onclick="redirect()">Try Again?</button>
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>5-1</title>
		<script></script>
	</head>
	<body>
		<fieldset>
			<legend align="center">Quiz</legend>
			<form action="5-1-1.php" method="post">
			<?php
	        	$servername = "localhost";
				$username = "root";
				$password = "";
				$database = "test";
	        	//Create connection
				$conn = new mysqli($servername, $username, $password, $database);
				//Check connection
				if ($conn->connect_error) die("Connection Failed: ". $conn->connect_error);
				// echo "Connection Successful <br>";

				$sql = "SELECT id, question, answer, choice1, choice2 FROM quiz";
				$result = $conn->query($sql);
				$i = 1;
				if ($result->num_rows > 0):
				    // output data of each row
				    while($row = $result->fetch_assoc()):
				    	$j = rand(1,3); 
				    	if ($j == 1):
			    			echo "<p>".$i.") ". $row["question"] ."</p>";
						  	echo '<input type="radio" name="num'. $i .'" value="'. $row["answer"] .'">'. $row["answer"] .'<br>';
						  	echo '<input type="radio" name="num'. $i .'" value="'. $row["choice1"] .'">'. $row["choice1"] .'<br>';
						  	echo '<input type="radio" name="num'. $i .'" value="'. $row["choice2"] .'">'. $row["choice2"] .'<br><br>';
			    		elseif ($j == 2):
			    			echo "<p>".$i.") ". $row["question"] ."</p>";
			    			echo '<input type="radio" name="num'. $i .'" value="'. $row["choice2"] .'">'. $row["choice2"] .'<br>';
						  	echo '<input type="radio" name="num'. $i .'" value="'. $row["answer"] .'">'. $row["answer"] .'<br>';
						  	echo '<input type="radio" name="num'. $i .'" value="'. $row["choice1"] .'">'. $row["choice1"] .'<br><br>';
		    			elseif ($j == 3):
		    				echo "<p>".$i.") ". $row["question"] ."</p>";
						  	echo '<input type="radio" name="num'. $i .'" value="'. $row["choice1"] .'">'. $row["choice1"] .'<br>';
		    				echo '<input type="radio" name="num'. $i .'" value="'. $row["choice2"] .'">'. $row["choice2"] .'<br>';
						  	echo '<input type="radio" name="num'. $i .'" value="'. $row["answer"] .'">'. $row["answer"] .'<br><br>';
			    		endif;
						$i++;
		        	endwhile;
				endif;
				$conn->close();
			?>
				<input type="submit" name="submit" value="Submit">
			</form>
		</fieldset>
	</body>
</html>

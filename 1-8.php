<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-8</title>
	</head>
	<body>
		<?php 
			$fName = "";
			$lName = "";
			$email = "";
			$age = "";
			$gender = "";
			$error = false;
			$output = "";
		?>
		<table align="center">
		    <tr>
		        <td>First name: </td>
		        <td><?php 
			        	if (empty($_POST["fName"])) {
			        		echo '<span style="color:red">First name is required</span>';
			        		$error = true;	
			        	} else if (is_numeric($_POST["fName"])) {
		        			echo '<span style="color:red">First name must have characters</span>';
		        			$error = true;
			        	} else {
		        			$fName = $_POST["fName"];
			        		echo $fName;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Last name:</td>
		        <td><?php 
			        	if (empty($_POST["lName"])) {
			        		echo '<span style="color:red">Last name is required</span>';
			        		$error = true;	
			        	} else if (is_numeric($_POST["lName"])) {
		        			echo '<span style="color:red">Last name must have characters</span>';
		        			$error = true;
			        	} else {
			        		$lName = $_POST["lName"];
			        		echo $lName;
			        	}
		        	?>		
		        </td>
		    </tr>

			<tr>
		        <td>Email address:</td>
		        <td><?php 
			        	if (empty($_POST["email"])) {
			        		echo '<span style="color:red">Email is required</span>';
			        		$error = true;	
			        	} else if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
		        			echo '<span style="color:red">Invalid email format</span>';
		        			$error = true;
			        	} else {
			        		$email = $_POST["email"];
			        		echo $email;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Age:</td>
		        <td><?php 
			        	if (empty($_POST["age"])) {
			        		echo '<span style="color:red">Age is required</span>';
			        		$error = true;	
			        	} else if (!is_numeric($_POST["age"])) {
		        			echo '<span style="color:red">Age must be numeric</span>';
		        			$error = true;
			        	} else {
			        		$age = $_POST["age"];
			        		echo $age;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Gender:</td>
		        <td><?php 
			        	if (empty($_POST["gender"])) {
			        		echo '<span style="color:red">Gender is required</span>';
			        		$error = true;	
			        	} else {
			        		$gender = $_POST["gender"];
			        		echo $gender;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		    <?php 
		    	if ($error == false) {
			     	$form_data = array(
					   'fName'  => $fName,
					   'lName'  => $lName,
					   'email'  => $email,
					   'age' => $age,
					   'gender' => $gender
				  	);
				  	$fp = fopen("person.csv", "a");
				  	fputcsv($fp, $form_data);
			     	fclose($fp);
			     	?><td><span style="color:red">Form was submitted successfully</span></td><?php
		    	} 
		    ?>
			</tr>

		    <tr>
		    	<form action="1-8.html">
		    		<td><input type="submit" value="Go back"></td>
				</form>
		    </tr>
		</table>
	</body>
</html>

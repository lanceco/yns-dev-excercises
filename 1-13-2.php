<!DOCTYPE html>
<?php 
	session_start();
	if(!isset($_SESSION['username'])) {
		header('location:1-13.php');
	}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-13-2</title>
	</head>
	<body>
		<form action="1-13-3.php" method="post" enctype="multipart/form-data">
			<table align="center">
			    <tr>
			        <td>Enter first name: </td>
			        <td><input type="text" name="fName"></td>
			    </tr>

			    <tr>
			        <td>Enter last name:</td>
			        <td><input type="text" name="lName"></td>
			    </tr>

				<tr>
			        <td>Enter email address:</td>
			        <td><input type="text" name="email"></td>
			    </tr>

			    <tr>
			        <td>Enter age:</td>
			        <td><input type="text" name="age"></td>
			    </tr>

			    <tr>
			        <td>Enter gender:</td>
			        <td><input type="radio" name="gender" value="Male" checked> Male</td>
  					<td><input type="radio" name="gender" value="Female"> Female</td>
			    </tr>

			    <tr>
			        <td>Select image to upload:</td>
			        <td><input type="file" name="fileToUpload" id="fileToUpload"></td>
			    </tr>			    

			    <tr>
			        <td></td>
			        <td><input type="submit" name="submit" value="                Submit                "></td>
			    </tr>
			</table>
		</form>
		<table align="center">
		    <tr>
		    	<td></td>
		        <td><a href="1-13-4.php">Records</a></td>
		    </tr>

		    <tr>
		    	<td></td>
		        <td><a href="1-13-5.php">Log out</a></td>
		    </tr>
		</table>
		<!-- <p>This is a paragraph.</p> -->
	</body>
</html>

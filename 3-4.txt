1) SELECT * from employees WHERE last_name LIKE 'K%'
2) SELECT * from employees WHERE last_name LIKE '%i'
3) SELECT CONCAT(first_name ,' ', middle_name ,' ', last_name) AS full_name,hire_date
	FROM employees 
    WHERE hire_date BETWEEN '2015-1-1' AND '2015-3-31' ORDER BY hire_date ASC
4) SELECT last_name as Employee, (SELECT last_name FROM employees WHERE id=1) as BOSS FROM employees WHERE boss_id=1
5) SELECT last_name FROM employees WHERE department_id='3' ORDER BY last_name DESC
6) SELECT COUNT(middle_name) AS count_has_middle FROM employees WHERE middle_name IS NOT NULL
7) SELECT departments.name AS department, COUNT(employees.department_id) AS number_of_employees 
	FROM employees 
    LEFT JOIN departments ON employees.department_id = departments.id
	GROUP BY name
8) SELECT first_name, middle_name, last_name, 
	CASE 
    	when hire_date IS NULL THEN hire_date
        ELSE MAX(hire_date)
	END AS hire_date
	FROM employees
9) SELECT name FROM departments LEFT JOIN employees ON departments.id = employees.department_id 
	WHERE employees.department_id IS NULL
10) SELECT first_name, middle_name, last_name FROM employees WHERE department_id AND boss_id IS NULL
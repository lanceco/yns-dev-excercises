<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-3</title>
	</head>
	<body>
		<form method="post">
			<table align="center">
			    <tr>
			        <td>Enter 1st Number</td>
			        <td><input type="number" step="any" name="n1"></td>
			    </tr>

			    <tr>
			        <td>Enter 2nd Number</td>
			        <td><input type="number" step="any" name="n2"></td>
			    </tr>

			    <tr>
			        <td></td>
			        <td><input type="submit" name="submit" value="                =                "></td>
			    </tr>
			    <?php 
			    	$result = "";
					if(isset($_POST['submit'])) {   
					    $result = gmp_gcd($_POST['n1'], $_POST['n2']); 
					}
				?>

			    <tr>
			        <td><strong><?= $result ?><strong></td>
			    </tr>
			</table>
		</form>
	</body>
</html>

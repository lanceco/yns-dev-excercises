<!DOCTYPE html>
<?php 
	session_start();
	if(!isset($_SESSION['username'])) {
		header('location:1-13.php');
	}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-13-3</title>
	</head>
	<body>
		<?php 
			$fName = "";
			$lName = "";
			$email = "";
			$age = "";
			$gender = "";
			$fileToUpload = "";
			$error = false;
			$output = "";

		?>
		<table align="center">
		    <tr>
		        <td>First name: </td>
		        <td><?php 
			        	if (empty($_POST["fName"])) {
			        		echo '<span style="color:red">First name is required</span>';
			        		$error = true;	
			        	} else if (is_numeric($_POST["fName"])) {
		        			echo '<span style="color:red">First name must have characters</span>';
		        			$error = true;
			        	} else {
		        			$fName = $_POST["fName"];
			        		echo $fName;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Last name:</td>
		        <td><?php 
			        	if (empty($_POST["lName"])) {
			        		echo '<span style="color:red">Last name is required</span>';
			        		$error = true;	
			        	} else if (is_numeric($_POST["lName"])) {
		        			echo '<span style="color:red">Last name must have characters</span>';
		        			$error = true;
			        	} else {
			        		$lName = $_POST["lName"];
			        		echo $lName;
			        	}
		        	?>		
		        </td>
		    </tr>

			<tr>
		        <td>Email address:</td>
		        <td><?php 
			        	if (empty($_POST["email"])) {
			        		echo '<span style="color:red">Email is required</span>';
			        		$error = true;	
			        	} else if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
		        			echo '<span style="color:red">Invalid email format</span>';
		        			$error = true;
			        	} else {
			        		$email = $_POST["email"];
			        		echo $email;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Age:</td>
		        <td><?php 
			        	if (empty($_POST["age"])) {
			        		echo '<span style="color:red">Age is required</span>';
			        		$error = true;	
			        	} else if (!is_numeric($_POST["age"])) {
		        			echo '<span style="color:red">Age must be numeric</span>';
		        			$error = true;
			        	} else {
			        		$age = $_POST["age"];
			        		echo $age;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Gender:</td>
		        <td><?php 
			        	if (empty($_POST["gender"])) {
			        		echo '<span style="color:red">Gender is required</span>';
			        		$error = true;	
			        	} else {
			        		$gender = $_POST["gender"];
			        		echo $gender;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Image:</td>
		        <td><?php
		        		if($_FILES["fileToUpload"]["error"] == 4) {
							//means there is no file uploaded
		        			echo '<span style="color:red">No image was uploaded.</span>';
		        			$error = true;
						} else {
				        	$target_dir = "uploads/";
							$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
							$uploadOk = 1;
							$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
							// Check if image file is a actual image or fake image
							if(isset($_POST["submit"])) {
							    $check =	 getimagesize($_FILES["fileToUpload"]["tmp_name"]);
							    if($check !== false) {
							        // echo "File is an image - " . $check["mime"] . ".";
							        $uploadOk = 1;
							    } else {
							        echo '<span style="color:red">File is not an image.</span>';
							        $uploadOk = 0;
							    }
							}

							// Check if file already exists
							if (file_exists($target_file)) {
							    echo '<span style="color:red">Sorry, file already exists.</span>';
							    $uploadOk = 0;
							    $error = true;
							}

							// Check file size
							if ($_FILES["fileToUpload"]["size"] > 500000) {
							    echo '<span style="color:red">Sorry, your file is too large.</span>';
							    $uploadOk = 0;
							    $error = true;
							}

							// Allow certain file formats
							if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
							&& $imageFileType != "gif" ) {
							    echo '<span style="color:red">Sorry, only JPG, JPEG, PNG & GIF files are allowed.</span>';
							    $uploadOk = 0;
							    $error = true;
							}

							// Check if $uploadOk is set to 0 by an error
							if ($uploadOk == 0) {
							    echo '<span style="color:red">Sorry, your file was not uploaded.</span>';
							    $error = true;

							// if everything is ok, try to upload file
							} else {
							   if ($error === false):
									if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
							        $fileToUpload = basename($_FILES["fileToUpload"]["name"]);
							        echo '<img src= "uploads/'. $fileToUpload .'" alt="Profile Image" height="100" width="100">';
							        echo "The file ". $fileToUpload. " has been uploaded.";

								    } else {
								         echo '<span style= "color:red">Sorry, there was an error uploading your file.</span>';
								         $error = true;
								    }
							    else: 
							    	echo '<span style= "color:red">File was not uploaded.</span>';
							    endif;
							}
						}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		    <?php
		    	if ($error == false) {
			     	$form_data = array(
					   'fName'  => $fName,
					   'lName'  => $lName,
					   'email'  => $email,
					   'age' => $age,
					   'gender' => $gender,
					   'fileToUpload' => $fileToUpload);

				  	$fp = fopen("person.csv", "a");
				  	fputcsv($fp, $form_data);
			     	fclose($fp);
			     	?><td><span style="color:green">Form was submitted successfully</span></td><?php
		    	} 
		    ?>
			</tr>
			<tr>
		    	<td><a href="1-13-4.php">Records</a></td>
		    </tr>
		    <tr>
		    	<form action="1-13-2.php">
		    		<td><input type="submit" value="Go back"></td>
				</form>
		    </tr>
		</table>
	</body>
</html>

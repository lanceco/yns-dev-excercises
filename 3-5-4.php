<!DOCTYPE html>
<?php 
	session_start();
	if(!isset($_SESSION['username'])) {
		header('location:3-5.php');
	}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>3-5-4</title>
		<script>
			function myFunction() {window.location.href = "3-5-2.php";}
		</script>
	</head>
	<body>
		<table align="center">
		    <tr>
	    		<td>No.</td>
		        <td>First name</td>
		        <td>Last name</td>
		        <td>Email</td>
		        <td>Age</td>
	        	<td>Gender</td>
	        	<td>Image</td>
		    </tr>
	        <?php
	        	$servername = "localhost";
				$username = "root";
				$password = "";
				$database = "test";
	        	//Create connection
				$conn = new mysqli($servername, $username, $password, $database);
				//Check connection
				if ($conn->connect_error) die("Connection Failed: ". $conn->connect_error);

				if (!isset($_GET['startrow']) or !is_numeric($_GET['startrow'])) {
					$startrow = 0;
				} else {
					$startrow = (int)$_GET['startrow'];
				}

				$fetch = mysqli_query($conn, "SELECT * FROM persons LIMIT ". $startrow .", 10") or die(mysqli_error());
				$num = mysqli_num_rows($fetch);

				if ($num > 0) {
					for ($i = 0; $i < $num; $i++) {
						$row = mysqli_fetch_row($fetch);
						echo "<tr>";
						echo"<td>$row[0]</td>";
				        echo"<td>$row[1]</td>";
				        echo"<td>$row[2]</td>";
				        echo"<td>$row[3]</td>";
				        echo"<td>$row[4]</td>";
				        echo"<td>$row[5]</td>";
				        echo"<td><img src= uploads/". $row[6] ." alt='Profile Image' height='50' width='50'></td>";
						echo "</tr>";
					}
					echo "<tr><td><a href=". $_SERVER['PHP_SELF'] ."?startrow=". ($startrow + 10). ">Next</a></td></tr>";
				} else {
					echo '<tr><td><span style="color:red">No Records</span></td></tr>';
				}
				
				$prev = $startrow - 10;

				if ($prev >= 0) {
					echo "<tr><td><a href=". $_SERVER['PHP_SELF'] ."?startrow=". $prev .">Previous</a></td></tr>";
				}
				// $sql = "SELECT id, firstname, lastname, email, age, gender, image FROM persons";
				// $result = $conn->query($sql);

				// if ($result->num_rows > 0) {
				// 	while ($row = $result->fetch_assoc()) {
				// 		echo "<tr>
				// 			<td>". $row['id'] ."</td>
				// 			<td>". $row['firstname'] ."</td>
				// 			<td>". $row['lastname'] ."</td>
				// 			<td>". $row['email'] ."</td>
				// 			<td>". $row['age'] ."</td>
				// 			<td>". $row['gender'] ."</td>
				// 			<td><img src= uploads/". $row['image'] ." alt='Profile Image' height='50' width='50'></td>
				// 		</tr>";
				// 	}
				// } else {
				// 	echo '<tr><td><span style="color:red">No Records</span></td></tr>';
				// }
				$conn->close();
			?>
			<tr>
		    	<td><button onclick="myFunction()">Go Back</button></td>
		    </tr>
		</table>
	</body>
</html>
	
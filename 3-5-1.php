<!DOCTYPE html>
<?php 
	session_start();
	if (isset($_POST['submit'])) {
		$username = "admin";
		$password = "321";
		if ($_POST['user'] == $username && $_POST['pass'] == $password) {
			$_SESSION['username'] = $username;
			header('location:3-5-2.php');
		} 
	}
?>

<html>
	<head>
		<meta charset="UTF-8">
		<title>3-5-1</title>
	</head>
	<body>
		<table align="center">
		    <tr>
		        <td>User: </td>
		        <td>
	        		<?php
			        	if (empty($_POST["user"])) {
			        		echo '<span style="color:red">Username is required</span>';
			        		$error = true;	
			        	} else if (! $_POST["user"] == $username) {
		        			echo '<span style="color:red">Invalid user</span>';
			        	} else {
		        			$user = $_POST["user"];
			        		echo $user;
			        	}
		        	?>
		        </td>
		    </tr>

		    <tr>
		        <td>Password:</td>
		        <td>
	        		<?php 
			        	if (empty($_POST["pass"])) {
			        		echo '<span style="color:red">Password is required</span>';
			        		$error = true;	
			        	} else if (! $_POST["pass"] == $password) {
		        			echo '<span style="color:red">Invalid password</span>';
			        	} else {
		        			$pass = $_POST["pass"];
			        		echo $pass;
			        	}
		        	?>
	        	</td>
		    </tr>
		    <tr>
		    	<td><a href="1-13.php">Go Back</a></td>
		    </tr>
		</table>
		<!-- <p>This is a paragraph.</p> -->
	</body>
</html>

<!DOCTYPE html>
<?php 
	session_start();
	if(!isset($_SESSION['username'])) {
		header('location:1-13.php');
	}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-13-4</title> 
	</head>
	<body>
		<table align="center">
		    <tr>
		        <td>First name</td>
		        <td>Last name</td>
		        <td>Email</td>
		        <td>Age</td>
	        	<td>Gender</td>
	        	<td>Images</td>
		    </tr>
	        <?php
	        	$fp = fopen("person.csv", "r");
	        	//Store each line in $data
	        	while(!feof($fp)) {
				  $c = fgetc($fp);
				  if($c === false) break;
				  $data[] = fgetcsv($fp);
				}
	        	// var_dump($data); die;

	        	//Get current page
	        	$currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

	        	//Record per page
	        	$perPage = 10;

	        	$start = ($currentPage - 1) * ($perPage);
	        	$totalPages = count($data) / $perPage;

	        	//Slice array according to our page
	        	$data = array_slice($data, $start, $perPage);
        		
        		for($i = 0; $i < count($data); $i++) { 
		        	echo "<tr>";
		        		for($j = 0; $j < 6; $j++) {
		        			if ($j === 5) {
	        					echo '<td><img src="uploads/'. $data[$i][$j] .'" alt="Profile Image" height="50" width="50"></tr>';
		        			} else {
		        				echo '<td>'. $data[$i][$j] .'</td>';
	        				}
        				}
		            echo "</tr>";
		         }

			    echo '<tr><td>';
			    // Show Total Pages
			    for($i = 0; $i < $totalPages; $i++){
			        echo '<a href="?page='.($i+1).'"> '.($i+1).' </a>';
			    }
			    echo '</td</tr>';
			    fclose($fp);
			?>
			<form action="1-13-2.php">
	    		<tr><td><input type="submit" value="Go back"></td></tr>
			</form>
		</table>
	</body>
</html>

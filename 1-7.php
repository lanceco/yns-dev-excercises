<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-7</title>
	</head>
	<body>
		<?php  
			$first_name = $_POST['fName'];
			$last_name = $_POST['lName'];
			$email = $_POST['email'];
			$age = $_POST['age'];
			$gender = $_POST['gender'];
		?>
		<table align="center">
		    <tr>
		        <td>First name: </td>
		        <td><?php 
			        	if (empty($first_name)) {
			        		echo '<span style="color:red">First name is required</span>';	
			        	} else if (preg_match('/[^a-z\s-]/i', $first_name)) {
		        			echo '<span style="color:red">First name must have only letters, dash, and white spaces</span>';
			        	} else {
			        		echo $first_name;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Last name:</td>
		        <td><?php 
			        	if (empty($last_name)) {
			        		echo '<span style="color:red">Last name is required</span>';	
			        	} else if (is_numeric($last_name)) {
		        			echo '<span style="color:red">Last name must have characters</span>';
			        	} else {
			        		echo $last_name;
			        	}
		        	?>		
		        </td>
		    </tr>

			<tr>
		        <td>Email address:</td>
		        <td><?php 
			        	if (empty($email)) {
			        		echo '<span style="color:red">Email is required</span>';	
			        	} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		        			echo '<span style="color:red">Invalid email format</span>';
			        	} else {
			        		echo $email;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Age:</td>
		        <td><?php 
			        	if (empty($age)) {
			        		echo '<span style="color:red">Age is required</span>';	
			        	} else if (!is_numeric($age)) {
		        			echo '<span style="color:red">Age must be numeric</span>';
			        	} else if ($age < 0) {
			        		echo '<span style="color:red">Age cannot be negative</span>';
			        	} else {
			        		echo $age;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Gender:</td>
		        <td><?php 
			        	if (empty($gender)) {
			        		echo '<span style="color:red">Gender is required</span>';	
			        	} else {
			        		echo $gender;
			        	}
		        	?>		
		        </td>
		    </tr>		
		    <tr>
		    	<form action="1-7.html">
		    		<td><input type="submit" value="Go back"></td>
				</form>
		    </tr>
		</table>
	</body>
</html>

<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = [
        'Flash',
        'Auth' => [
            'loginRedirect' => [
                'controller' => 'posts',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'authenticate' => [
                'Form' => [
                    'userModel' => 'User',
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ],
                    'passwordHasher' => 'Blowfish'
                ],
                'Basic' => [
                    'userModel' => 'User',
                    'field' => [
                        'username' => 'username',
                        'password' => 'password'
                    ],
                    'passwordHasher' => 'Blowfish'
                ]
            ],
            'authorize' => ['Controller']
        ]
    ];

    public function isAuthorized($user) {
        // Admin can access every action
        // if (isset($user['role']) && $user['role'] === 'admin'):
        //     return true;
        // endif;

        //Default deny
        return false;
    }

    // public function beforeRender() {
    //     parent::beforeRender();

    //     if (AuthComponent::user()):
    //         $this->loadModel('User');
    //         $info = $this->User->findById($this->Auth->User('id'));
    //         $this->Auth->login($info['User']);
    //     endif;
    // }
    
    public function beforeFilter() 
    {
        parent::beforeFilter();

        //Allow not registered users to do the following actions
        $this->Auth->allow('add', 'logout', 'activation', 'forgotPassword', 'resetPassword', 'login', 'api_login');
    }
}

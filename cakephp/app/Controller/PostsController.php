<?php
App::uses('AppController', 'Controller');
class PostsController extends AppController 
{
    public $helpers = ['Html', 'Form'];
    public $components = ['Paginator'];

    public function isAuthorized($user)
    {
        //Logout users if not yet activated
        if (isset($user['activation']) && $user['activation'] === '0'):
            $this->Flash->error(__('The user has not been activated'));
            return $this->redirect(['controller' => 'users', 'action' => 'logout']);
        endif;

        //All registered users can do the following actions posts
        if ($this->action === 'add' || $this->action === 'index' 
            || $this->action === 'view' || $this->action === 'like'
            || $this->action === 'viewLike' || $this->action === 'share'
            || $this->action === 'search'):
            return true;
        endif;

        if (in_array($this->action, ['edit', 'delete'])):
            $getParam = (int) $this->request->params['pass'][0];
            // $postId = substr($id, 0, 1);
            if ($this->Post->isOwnedBy($getParam, $user['id'])):
                return true;
            endif;
        endif;

        if (in_array($this->action, ['deleteComment', 'editComment'])):
            $getParam = (int) $this->request->params['pass'][0];

            $this->loadModel('Comment');
            if($this->Comment->isOwnedByComment($getParam, $user['id'])):
                return true;
            endif;
        endif;

        return parent::isAuthorized($user);
    }
    
    public function index() 
    {
        // $test = $this->Paginator->paginate(['Post.deleted' => '0']);
        // var_dump($test); die;
        $this->set([
            'posts' => $this->Paginator->paginate(['Post.deleted' => '0']),
            'user' => $this->Auth->user()]);
    }

    public function search()
    {
        // if(empty($this->request->data['User']['keyword'])):
        //     // $conditions = "";
        //     $this->Flash->error('Please input a username to search');
        //     $this->redirect(['controller' => 'posts', 'action' => 'index']);
        // endif;

        if(!empty($this->request->is('post'))):
            $cond=[];
            // $cond['User.activation'] = '1';
			$cond['User.username LIKE'] = "%" . trim($this->request->data['User']['keyword']) . "%";
			// $cond['User.last_name LIKE'] = "%" . trim($this->request->data['User']['keyword']) . "%";
            // $cond['User.email LIKE'] = "%" . trim($this->request->data['User']['keyword']) . "%";
            $conditions['AND'] = $cond;
            
            $this->request->params['named']['User.keyword'] = $this->request->data['User']['keyword'];
        else:
            $conditions = '';
		endif;
        
		 $this->paginate = ['conditions' => $conditions, 'limit' => '6'];
         $results = $this->Paginator->paginate('User');
         $this->set(['results'=> $results , 'user' => $this->Auth->user()]);
    }

    public function view($id = null) 
    {   
        if (!$id) throw new NotFoundException(__('Invalid post'));

        $post = $this->Post->findById($id);
        $sharePostId = $post['Post']['share_post_id'];
        $sharePost = $this->Post->findById($sharePostId);
        array_map([$this, 'loadModel'], ['Comment', 'Like']);
        $likes = $this->Like->findAllByPostIdAndDeleted($id, '0');
        // $comments = $this->Comment->findAllByPostIdAndDeleted($id, '0');
        // $this->paginate = [
        //     'Comment' => [
        //         'conditions' => ['Comment.post_id' => $id, 'Comment.deleted' => '0'],
        //         'limit' => 5,
        //         'paramType' => 'querystring'
        //     ]
        // ];
        // $comments = $this->paginate('Comment');

        // var_dump($comments); die;

        if (!$post) throw new NotFoundException(__('Invalid post'));

        $this->set(['post' => $post, 'sharePost' => $sharePost, 'likes' => $likes, 'user' => $this->Auth->user()]);

        if ($this->request->is('post')):
            array_map([$this, 'loadModel'], ['Comment', 'User']);

            $currentUserId = $this->Auth->user('id');
            $user = $this->User->findById($currentUserId);

            $this->Comment->create();
            $this->request->data['Comment']['post_id'] = $id;
            $this->request->data['Comment']['user_id'] = $currentUserId;
            $this->request->data['Comment']['username'] = $user['User']['username'];

            if ($this->Comment->save($this->request->data)): 
                $this->Flash->success(__('Your comment has been saved.'));
                $this->redirect(['controller' => 'posts', 'action' => 'view', $id]);
            endif;
            $this->Flash->error(__('Your comment was not been saved.'));
        endif;
    }

    public function editComment($id = null, $postId = null)
    {
        $this->loadModel('Comment');
        $this->Comment->id = $this->Comment->findById($id);
        // $this->request->data = $this->Comment->findById($id);
        $this->set('user', $this->Auth->user());

        if ($this->request->is('post')):
            if($this->Comment->save($this->request->data)):
                $this->Flash->success(__('Your comment has been updated'));
                $this->redirect(['controller' => 'posts', 'action' => 'view', $postId]);
            endif;
        endif;
    }

    public function deleteComment($id = null, $postId = null)
    {
        $this->loadModel('Comment');

        $this->Comment->updateAll(
            ['Comment.deleted' => '1'],
            ['Comment.id' => $id]);

        // $data = ['id' => $id, 'deleted' => '1'];
        // $this->Comment->save($data);
        
        $this->Flash->success(__('Your comment has been deleted'));
        $this->redirect(['controller' => 'posts', 'action' => 'view', $postId]);
    }

    public function like($id = null)
    {
        $this->loadModel('Like');
        $currentUserId = $this->Auth->user('id');
        //Use hasAny
        $post = $this->Post->findById($id);
        foreach ($post['Like'] as $like):
            if ($like['user_id'] === $currentUserId && $like['deleted'] === '0'):
                $this->Like->updateAll(
                    ['Like.deleted' => '1'],
                    ['Like.user_id' => $currentUserId, 'Like.post_id' => $id]);
                $this->Flash->error('You unliked the post');
                $this->redirect(['controller' => 'posts', 'action' => 'view', $id]);
            elseif ($like['user_id'] === $currentUserId && $like['deleted'] === '1'):
                $this->Like->updateAll(
                    ['Like.deleted' => '0'],
                    ['Like.user_id' => $currentUserId, 'Like.post_id' => $id]);
                $this->Flash->success('You liked the post');
                $this->redirect(['controller' => 'posts', 'action' => 'view', $id]);
            endif;
        endforeach;

        $this->Like->create();
        $this->request->data['Like']['post_id'] = $id;
        $this->request->data['Like']['user_id'] = $this->Auth->user('id');

        if ($this->Like->save($this->request->data)):
            $this->Flash->success('You liked the post');
            $this->redirect(['controller' => 'posts', 'action' => 'view', $id]);
        endif;
        $this->Flash->error(__('Your like was not been saved.'));
        $this->redirect(['controller' => 'posts', 'action' => 'view', $id]);
    }

    public function viewLike($id = null)
    {
        $this->loadModel('Like');
        $likes = $this->Like->findAllByPostIdAndDeleted($id, '0');
        $this->set(['likes' => $likes, 'user' => $this->Auth->user()]);
    }

    public function share($id = null)
    {
        $currentUserId = $this->Auth->user('id');
        $post = $this->Post->findById($id);
        $sharePostId = $post['Post']['share_post_id'];
        $checkShare = $this->Post->findAllBySharePostIdAndUserIdAndDeleted($id, $currentUserId, '0');
        $checkShare1 = $this->Post->findAllBySharePostIdAndUserIdAndDeleted($sharePostId, $currentUserId, '0');
        
        if (count($checkShare) === 1 || count($checkShare1) === 1):
            $this->Flash->error(__('You already shared this post.'));
            return $this->redirect(['action' => 'index']);
        endif;

        $this->set(['post' => $post, 'user' => $this->Auth->user()]);

        if ($this->request->is('post')):
            $this->Post->create();
            $this->request->data['Post']['share_post_id'] = $id;
            $this->request->data['Post']['user_id'] = $currentUserId;

            if ($this->Post->save($this->request->data)):
                $this->Flash->success(__('The post has been shared.'));
                return $this->redirect(['action' => 'index']);
            endif;
            $this->Flash->success(__('Your post has not been shared.'));
        endif;
    }
    
    public function add() 
    {
        $this->loadModel('User');
        $currentUserId = $this->Auth->user('id');
        $user = $this->User->findById($currentUserId);

        $this->set('user', $user);

        if ($this->request->is('post')):
            $fileArray = $this->request->data['Post']['blog_pic'];

            $this->Post->create();
            $this->request->data['Post']['user_id'] = $currentUserId;

            if ($this->Post->save($this->request->data)):
                $postId = $this->Post->getInsertID();
                $this->loadModel('Image');

                if (count($fileArray) > 0):
                    for ($i = 0; $i < count($fileArray); $i++):
                        $this->Image->create();
        
                        $fileName = $fileArray[$i]['name'];
                        $fileNameHash = Security::hash(Security::randomBytes(32).time(), 'md5', true);
                        $fileTmpLoc = $fileArray[$i]['tmp_name'];
                        $fileError = $fileArray[$i]['error'];
                        $fileExt = explode('.', $fileName);
                        $fileActualExt = strtolower(end($fileExt));
                        $allowedExt = ['jpg', 'jpeg', 'png'];
                        $newFileName = $fileNameHash.'.'.$fileActualExt;
                        $filePath = 'uploads/'. $newFileName;
                        // $fileSize = $fileArray[$i]['size'];
                        // $fileType = $fileArray[$i]['type'];
        
                        if ($fileError === 0):
                            if(in_array($fileActualExt, $allowedExt)):
                                $this->request->data['Image']['post_id'] = $postId;
                                $this->request->data['Image']['filename'] = $newFileName;
                                if ($this->Image->save($this->request->data)):
                                    move_uploaded_file($fileTmpLoc, WWW_ROOT.$filePath);
                                else:
                                    $this->Flash->error(__('Unable to add image to post: '. $fileName));
                                endif;
                            else:
                                $this->Flash->error(__($fileActualExt .' :Invalid file extension: '. $fileName. ' :Only jpeg, jpg or png files'));
                            endif;
                        else:
                            // file error is 4 if no files are uploaded
                            // $this->Flash->error(__($fileError .': Unable to add image to post: '. $fileName));
                        endif;
                    endfor;
                endif;
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(['action' => 'index']);
            endif;
            $this->Flash->error(__('Unable to add your post.'));
        endif;
    }

    public function edit($id = null)
    {
        if (!$id) throw new NotFoundException(__('Invalid Post'));

        // $post = $this->Post->findByPostId($id);
        $post = $this->Post->findById($id);
        // var_dump($post); die;
        if (!$post)  throw new NotFoundException(__('Invalid Post'));

        // $this->loadModel('Image');
        // $images = $this->Image->findAllByPostIdAndDeleted($id, '0');
        $this->set(['post' => $post, 'user' => $this->Auth->user()]);

        if ($this->request->is(['post', 'put'])):
            $selected = $this->request->data['Post']['edit_blog_pic'];
            $fileArray = $this->request->data['Post']['blog_pic'];
            $this->Post->id = $id;

            if ($this->Post->save($this->request->data)):
                $this->loadModel('Image');

                if (count($selected) > 0):
                    for ($i = 0; $i < count($selected)-1; $i++):
                        $this->Image->updateAll(
                            ['deleted' => '1'],
                            ['filename' => $selected[$i]]);
                    endfor;
                endif;

                if (count($fileArray) > 0):
                    for ($i = 0; $i < count($fileArray); $i++):
                        $this->Image->create();
        
                        $fileName = $fileArray[$i]['name'];
                        $fileNameHash = Security::hash(Security::randomBytes(32).time(), 'md5', true);
                        $fileTmpLoc = $fileArray[$i]['tmp_name'];
                        $fileError = $fileArray[$i]['error'];
                        $fileExt = explode('.', $fileName);
                        $fileActualExt = strtolower(end($fileExt));
                        $allowedExt = ['jpg', 'jpeg', 'png'];
                        $newFileName = $fileNameHash.'.'.$fileActualExt;
                        $filePath = 'uploads/'. $newFileName;
                        // $fileSize = $fileArray[$i]['size'];
                        // $fileType = $fileArray[$i]['type'];
        
                        if ($fileError === 0):
                            if(in_array($fileActualExt, $allowedExt)):
                                $this->request->data['Image']['post_id'] = $id;
                                $this->request->data['Image']['filename'] = $newFileName;
                                if ($this->Image->save($this->request->data)):
                                    move_uploaded_file($fileTmpLoc, WWW_ROOT.$filePath);
                                else:
                                    $this->Flash->error(__('Unable to add image to post: '. $fileName));
                                endif;
                            else:
                                $this->Flash->error(__($fileActualExt .' :Invalid file extension: '. $fileName . ' :Only jpeg, jpg or png files'));
                            endif;
                        else:
                            // file error is 4 if no files are uploaded
                            // $this->Flash->error(__($fileError .': Unable to add image to post: '. $fileName));
                        endif;
                    endfor;
                endif;
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(['action' => 'index']);
            endif;
            $this->Flash->error(__('Unable to update your post.'));
        endif;

        if (!$this->request->data) $this->request->data = $post;
    }

    public function delete($id)
    {
        if ($this->request->is('get')) throw new MethodNotAllowedException();

        $this->Post->updateAll(
            ['Post.deleted' => '1'],
            ['Post.id' => $id]);

        $this->Flash->success(__('The post was deleted.'));
        return $this->redirect(['action' => 'index']);
    }
}
?>
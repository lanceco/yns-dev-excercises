<?php 
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController
{
    public $components = [
        'RequestHandler'
    ];

    public function isAuthorized($user)
    {
        //All registered users can do the following actions users
        if ($this->action === 'edit' || $this->action === 'changePassword' 
            || $this->action === 'changeAccount' || $this->action === 'changeProfile' 
            || $this->action === 'view'|| $this->action === 'follow' || $this->action === 'viewFollowers'
            || $this->action === 'viewFollowing'):
            return true;
        endif;
        
        return parent::isAuthorized($user);
    }

    public function api_login()
    {
        $this->layout = false;
        $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
        pr($this->request->data); die;
        if($this->request->is('post')) {
            $data = $this->request->data('json_decode', true);
            pr($data); die;
            // if ($this->request->header('x-api-key') === Configure::read('x-api-key')) {
            //     echo 'NICE'; die;
            //     //get data from request object
            //     $data = $this->request->input('json_decode', true);
            // }
        }
                
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function login() 
    {
        if ($this->Auth->loggedIn()):
            $this->redirect(['controller' => 'posts', 'action' => 'index']);
        endif;

        if ($this->request->is('post')):
            if ($this->Auth->login()) return $this->redirect($this->Auth->redirectUrl());

            $this->Flash->error(__('Invalid username or password, try again'));
        endif;
    }

    public function logout() 
    {
        return $this->redirect($this->Auth->logout());
    }

    public function follow($id = null)
    {
        $this->loadModel('Follow');
        $this->Follow->id = $id;

        $currentUserId = $this->Auth->user('id');
        $user = $this->User->findById($currentUserId);
        foreach ($user['Follow'] as $follow):
            if ($follow['following_id'] === $id && $follow['deleted'] === '0'):
                $this->Follow->updateAll(
                    ['deleted' => '1'],
                    ['user_id' => $currentUserId, 'following_id' => $id]);
                $this->Flash->error(__('Unfollowed user'));
                $this->redirect(['controller' => 'users', 'action' => 'view', $id]);
            elseif ($follow['following_id'] === $id && $follow['deleted'] === '1'):
                $this->Follow->updateAll(
                    ['deleted' => '0'],
                    ['user_id' => $currentUserId, 'following_id' => $id]);
                $this->Flash->success(__('Followed user'));
                $this->redirect(['controller' => 'users', 'action' => 'view', $id]);
            endif;
        endforeach;

        $this->Follow->create();
        $this->request->data['Follow']['following_id'] = $id; 
        $this->request->data['Follow']['user_id'] = $currentUserId;
        if ($this->Follow->save($this->request->data)):
            $this->Flash->success(__('Followed user saved'));
            $this->redirect(['controller' => 'users', 'action' => 'view', $id]);
        endif;
        $this->redirect(['controller' => 'users', 'action' => 'view', $id]);
    }

    public function viewFollowers($id = null)
    {
        $this->loadModel('Follow');
        $this->Follow->bindModel([
            'belongsTo' => [
                'User' => [
                    'classname' => 'User', 
                    'foreignKey' => 'user_id'
                ]
            ]
        ]);

        $followers = $this->Follow->findAllByFollowingIdAndDeleted($id, '0');
        $currentUserId = $this->Auth->user('id');
        $user = $this->User->findById($currentUserId);

        $this->set(['followers' => $followers, 'user' => $user]);
        // var_dump($followers); die;
    }

    public function viewFollowing($id = null)
    {
        $this->loadModel('Follow');
        $this->Follow->bindModel([
            'belongsTo' => [
                'User' => [
                    'classname' => 'User', 
                    'foreignKey' => 'following_id'
                ]
            ]
        ]);
        
        $following = $this->Follow->findAllByUserIdAndDeleted($id, '0');
        $currentUserId = $this->Auth->user('id');
        $user = $this->User->findById($currentUserId);

        $this->set(['following' => $following, 'user' => $user]);
    }

    public function view($id = null) 
    {
        $this->User->id = $id;
        if (!$this->User->exists()) throw new NotFoundException(__('Invalid user'));

        $currentUser = $this->User->findById($this->Auth->user('id'));
        $user = $this->User->findById($id);
        // var_dump($user); die;
        
        $this->set(['user' => $user, 'currentUser' => $currentUser]);
    }

    public function forgotPassword()
    {
        if ($this->request->is('post')):
            $email = $this->request->data['User']['email'];
            $user = $this->User->findByEmail($email);

            if ($user != null):
                $userName = $user['User']['username'];
                $userEmail = $user['User']['email'];
                $userToken = $user['User']['token'];
                
                $email = new CakeEmail('gmail');
                $email->emailFormat('html');
                $email->from(['me@example.com' => 'My Site']);
                $email->to($userEmail);
                $email->subject('Reset Password');
                $email->send('Hello '. $userName .'!</br>
                    To reset password, click the link below</br>
                    <a href="http://dev1.ynsdev.pw/cakephp/users/resetPassword/'. $userToken .'">Reset password</a>');

                $this->Flash->success(__('An email has been sent to reset password'));
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            endif;
            $this->Flash->error(__('Email was not found in the database, try again.'));
        endif;
    }

    public function resetPassword($token = null)
    {
        if ($this->request->is('post') || $this->request->is('put')):
            $user = $this->User->findByToken($token);
            $id = $user['User']['id'];
            $this->User->id = $id;

            if ($this->User->save($this->request->data)):
                $this->Flash->success(__('Reset password was successful'));
                $this->redirect(['controller' => 'users', 'action' => 'login']);
            endif;
        endif;
    }

    public function activation($token = null) 
    {
        $this->User->updateAll(['activation' => '1'], ['token' => $token]);
        $this->Flash->success(__('The user has been activated'));
        $this->redirect(['controller' => 'users', 'action' => 'login']);
    }

    public function add() 
    {   
        if ($this->request->is('post')):
            $this->User->create();

            $userToken = Security::hash(Security::randomBytes(32).time(), 'md5', true);
            $userEmail = $this->request->data['User']['email'];
            $userName = $this->request->data['User']['username'];

            $this->request->data['User']['token'] = $userToken;

            if ($this->User->save($this->request->data)):
                $email = new CakeEmail('gmail');
                $email->emailFormat('html');
                $email->from(['me@example.com' => 'My Site']);
                $email->to($userEmail);
                $email->subject('Activate account');
                $email->send('Hello '. $userName .'!</br>
                    Please activate your account in the link below</br>
                    <a href="http://dev1.ynsdev.pw/cakephp/users/activation/'. $userToken .'">Activate account</a>');

                $this->Flash->success(__('The user has been saved and an email has been sent'));
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            endif;

            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        endif;
    }
    
    public function edit() 
    {
        $this->User->id = $this->Auth->user('id');

        if (!$this->User->exists()) throw new NotFoundException(__('Invalid user'));
    }

    public function changeAccount() 
    {
        $this->User->id = $this->Auth->user('id');

        if (!$this->User->exists()) throw new NotFoundException(__('Invalid user'));

        if ($this->request->is('post') || $this->request->is('put')):
            if ($this->User->save($this->request->data)):
                $this->Flash->success(__('Your account has been updated'));
                return $this->redirect(['controller' => 'users', 'action' => 'edit']);
            endif;
            $this->Flash->error(__('The account could not be updated. Please, try again.'));
        endif;
    }

    public function changeProfile()
    {
        $currentUserId = $this->Auth->user('id');
        $user = $this->User->findById($currentUserId);
        $currentProfilePic = $user['User']['profile_pic'];

        $this->User->id = $currentUserId;

        if ($this->request->is('post') || $this->request->is('put')):
            $file = $this->request->data['User']['profile_pic'];
            // var_dump($file); die;
            $fileName = $file['name'];
            $fileNameHash = Security::hash(Security::randomBytes(32).time(), 'md5', true);
            $fileTmpLoc = $file['tmp_name'];
            $fileError = $file['error'];
            $fileExt = explode('.', $fileName);
            $fileActualExt = strtolower(end($fileExt));
            $allowedExt = ['jpg', 'jpeg', 'png'];
            $newFileName = $fileNameHash .'.'.$fileActualExt;
            $filePath = 'uploads/'. $newFileName;
            $fileSize = $file['size'];
            // $fileType = $file['type'];

            if ($fileSize > 0):
                if ($fileError === 0):
                    if(in_array($fileActualExt, $allowedExt)):
                        move_uploaded_file($fileTmpLoc, WWW_ROOT.$filePath);
                        $this->request->data['User']['profile_pic'] = $newFileName;
                    else:
                        $this->Flash->error(__($fileActualExt .': Invalid file extension: '. $fileName . ' Only jpeg, jpg or png files'));
                        return $this->redirect(['controller' => 'users', 'action' => 'edit']);
                    endif;
                else:
                    // $this->Flash->error(__($fileError .': Unable to add image to post: '. $fileName));
                    // file error is equal to 4 if no files are uploaded
                endif;
            else:
                $this->request->data['User']['profile_pic'] = $currentProfilePic;
            endif;
            
            if ($this->User->save($this->request->data)):
                $this->Flash->success(__('The user has been updated'));
                return $this->redirect(['controller' => 'users', 'action' => 'edit']);
            endif;
            $this->Flash->error(__('The profile could not be updated. Please, try again.'));
        endif;
    }
}
?>
<?php 
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel 
{
    public $hasMany = [
        'Post' => [
            'classname' => 'Post',
            'foreignKey' => 'user_id',
            'conditions' => ['Post.deleted = 0']
        ],
        'Follow' => [
            'classname' => 'Follow',
            'foreignKey' => 'user_id'
        ]
    ];

    // public $hasAndBelongsToMany = [
    //     'userFollow' => [
    //         'className' => 'User',
    //         'joinTable' => 'follows',
    //         'foreignKey' => 'user_id',
    //         'associationForeignKey' => 'follow_user_id'
    //     ],

    //     'userFollowing' => [
    //         'className' => 'User',
    //         'joinTable' => 'follows',
    //         'foreignKey' => 'follow_user_id',
    //         'associationForeignKey' => 'user_id'
    //     ],

    // ];

    public $validate = [
        'username' => [
            'required' => [
                'rule' => ['isUnique', 'username', false],
                'message' => 'This username has already been used.'
            ],
            'alphaNumeric' => [
                'rule' => 'alphaNumeric',
                'required' => false,
                'message' => 'Letters and numbers only'
            ]
        ],
        'password' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'A password is required'
            ]
        ],
        'confirm_password' => [
            'required' => [
                'rule' => ['equalToField'],
                'message' => 'Passwords did not match'
            ]
        ],
        'first_name' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'First name is required'
            ],
            'alphaNumeric' => [
                'rule' => 'alphaNumeric',
                'required' => false,
                'message' => 'Letters and numbers only'
            ]
        ],
        'last_name' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'First name is required'
            ],
            'alphaNumeric' => [
                'rule' => 'alphaNumeric',
                'required' => false,
                'message' => 'Letters and numbers only'
            ]
        ],
        'email' => [
            'required' => [
                'rule' => ['isUnique', 'email', false],
                'message' => 'This email has already been used.'
            ]
        ]
    ];

    public function equalToField() {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['confirm_password'];
    }
    
    public function beforeSave($options = array()) 
    {
        $passwordHasher = new BlowfishPasswordHasher();
        
        if (isset($this->data[$this->alias]['password'])):
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']);
        endif;
        
        return true;
    }
}
?>
<?php
class Like extends AppModel 
{
    public $belongsTo = [
        'User' => [
            'classname' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Post' => [
            'classname' => 'Post',
            'foreignKey' => 'post_id'
        ]
    ];
}
?>
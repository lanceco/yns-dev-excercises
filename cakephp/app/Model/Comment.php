<?php
class Comment extends AppModel 
{
    public $belongsTo = [
        'User' => [
            'classname' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Post' => [
            'classname' => 'Post',
            'foreignKey' => 'post_id'
        ]
    ];

    public $validate = [
        'message' => [
            'required' => [
                'rule' => ['maxLength', 140],
                'message' => 'Message must not be longer than 140 characters'
            ],
            'alphaNumeric' => [
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ]
        ] 
    ];

    public function isOwnedByComment($comment, $user) {
        return $this->field('id', ['id' => $comment, 'user_id' => $user]) !== false;
    }
}
?>
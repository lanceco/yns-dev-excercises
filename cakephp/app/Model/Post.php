<?php
class Post extends AppModel 
{
    public $belongsTo = [
        'User' => [
            'classname' => 'User',
            'foreignKey' => 'user_id'
        ]
    ];

    public $hasMany = [
        'Like' => [
            'classname' => 'Like',
            'foreignKey' => 'post_id'
        ],
        'Comment' => [
            'classname' => 'Comment',
            'foreignKey' => 'post_id',
            'conditions' => ['Comment.deleted = 0']
        ],
        'Image' => [
            'classname' => 'Image',
            'foreignKey' => 'post_id',
            'conditions' => ['Image.deleted = 0']
        ]
    ];
    // public $hasMany = [
    //     'Customer' => [
    //         'className' => 'Customer',
    //         'foreignKey' => 'customer_id',
    //         'conditions' => [
    //             'Customer.deleted = 0'
    //         ]
    //     ],
    // ];
    // ,

    public $validate = [
        'title' => [
            'required' => [
                'rule' => ['maxLength', 40],
                'message' => 'Title must not be longer than 40 characters'
            ],
            'alphaNumeric' => [
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ]
            
        ],
        'message' => [
            'required' => [
                'rule' => ['maxLength', 140],
                'message' => 'Message must not be longer than 140 characters'
            ],
            'alphaNumeric' => [
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ]
        ] 
    ];

    public function isOwnedBy($post, $user) {
        return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    }
}
?>
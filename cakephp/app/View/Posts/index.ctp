<div class='actions' style='display: inline-block'>
	<h3 style="text-align:center;"><?=  h($user['first_name']) ." ". h($user['last_name']) ?></h3> 
	<div style="margin-left: 15px;">
	    <?= $this->Html->image('/uploads/'. h($user['profile_pic']), ['height' => '150px' , 'width' => '180px']) ?>
    </div>
    <div>
		<ul>
			<li><?= $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Followers'), 
                        ['controller' => 'users', 'action' => 'viewFollowers', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Following'), 
                        ['controller' => 'users', 'action' => 'viewFollowing', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Add post'), 
                        ['controller' => 'posts', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Edit Account'), 
                        ['controller' => 'users', 'action' => 'edit']); ?></li>
            <li><?= $this->Html->link(__('Logout'), 
                        ['controller' => 'users', 'action' => 'logout']); ?></li>
		</ul>
	</div>
</div>
<div class="users form" style='display: inline-block'>
    <?php 
	echo $this->Form->create('User', ['url' => 'search']); 
	echo $this->Form->input('keyword', [
        'class' => 'form-control input-sm',
        'maxlength' => "64", 
        'placeholder' => ' Search users here...', 
        'div'=>false, 
        'label'=>false
    ]); 
	echo $this->Form->submit('Search');
	echo $this->Form->end(); 
	?>
    <table>
        <tr>
            <th style="text-align:center !important;"><?= $this->Paginator->sort('Posts'); ?></th>
			<th style="text-align:center !important;"><?= $this->Paginator->sort('Posted by'); ?></th>
			<th style="text-align:center !important;"><?= $this->Paginator->sort('Posted time'); ?></th>
			<th style="text-align:center !important;"><?= $this->Paginator->sort('Actions'); ?></th>
        </tr>
        <?php foreach ($posts as $post): ?>
        <tr>
            <td>
                <?php 
                echo $this->Html->link(
                    h($post['Post']['title']),
                    ['controller' => 'posts','action' => 'view', 
                    $post['Post']['id']]
                ); 
                ?>
            </td>
            <td>
                <?= $this->Html->image(
                    '/uploads/'. h($post['User']['profile_pic']), ['height' => '30px' , 'width' => '30px'])?>
                <?php 
                echo $this->Html->link(
                    h($post['User']['username']),
                    ['controller' => 'users','action' => 'view', 
                    $post['User']['id']]
                ); 
                ?>
            </td>
            <td><?php echo $post['Post']['created']; ?></td>
            <td class='actions'>
                <?php
                if (AuthComponent::user('id') === $post['User']['id']): 
                    echo $this->Form->postlink(
                        'Delete',
                        ['controller' => 'posts', 'action' => 'delete', $post['Post']['id']],
                        ['confirm' => 'Are you sure?']
                    ); 
                    echo $this->Html->link(
                        'Edit',
                        ['controller' => 'posts', 'action' => 'edit', $post['Post']['id']]
                    );
                endif; 
                echo $this->Html->link(
                    'Share',
                    ['controller' => 'posts', 'action' => 'share', $post['Post']['id']],
                    ['confirm' => 'Are you sure?']
                );
                // echo $this->Html->link(
                //     'Like',
                //     ['action' => 'like', $post['Post']['id']]
                // );  
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php unset($post); ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter([
            'format' => __(
                'Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ]);
        ?>	
    </p>
    <div class="paging">
	    <?php
		echo $this->Paginator->prev('< ' . __('previous'), [], null, ['class' => 'prev disabled']);
		echo $this->Paginator->numbers(['separator' => ' ']);
		echo $this->Paginator->next(__('next') . ' >', [], null, ['class' => 'next disabled']);
	    ?>
    </div>
</div>

    



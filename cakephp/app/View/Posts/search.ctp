<!-- File: /app/View/Posts/index.ctp -->
<div class='actions' style='display: inline-block'>
	<h3 style="text-align:center;"><?=  h($user['first_name']) ." ". h($user['last_name']) ?></h3> 
	<div style="margin-left: 15px;">
	    <?= $this->Html->image('/uploads/'. h($user['profile_pic']), ['height' => '150px' , 'width' => '180px'])?>
    </div>
    <div>
		<ul>
			<li><?php echo $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
            <li><?php echo $this->Html->link(__('Edit Profile'), 
                ['controller' => 'users', 'action' => 'edit']); ?></li>
            <li><?php echo $this->Html->link(__('Followers'), 
                array('controller' => 'users', 'action' => 'viewFollowers', $user['id'])); ?></li>
            <li><?php echo $this->Html->link(__('Following'), 
                array('controller' => 'users', 'action' => 'viewFollowing', $user['id'])); ?></li>
            <li><?php echo $this->Html->link(__('Add post'), 
                ['controller' => 'posts', 'action' => 'add']); ?></li>
            <!-- <li><?php echo $this->Html->link(__('View Your posts'), 
                array('controller'=>'users','action' => 'view',$logId)); ?></li> -->
            <li><?php echo $this->Html->link(__('Logout'), 
                ['controller' => 'users', 'action' => 'logout']); ?></li>
		</ul>
	</div>
</div>
<div class="users form" style='display: inline-block'>
    <?php 
	echo $this->Form->create('User',['url' => 'search']); 
	echo $this->Form->input('keyword',['class'=>'form-control input-sm','maxlength'=>"64",'placeholder'=>' Search user here...','div'=>false,'label'=>false]); 
	echo $this->Form->submit('Search');
    echo $this->Form->end();
	?>
    <table>
        <tr>
            <th style="text-align:center !important;"><?= $this->Paginator->sort('User'); ?></th>
        </tr>
        <?php foreach ($results as $result): ?>
        <tr>
            <td>
                <?php 
                echo $this->Html->link(
                    h($result['User']['username']),
                    ['controller' => 'users','action' => 'view', 
                    $result['User']['id']]
                ); 
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php unset($post); ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	
    </p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>

    



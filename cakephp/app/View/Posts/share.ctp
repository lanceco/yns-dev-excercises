<div class='actions' style='display: inline-block'>
	<h3 style="text-align:center;"><?=  h($user['first_name']) ." ". h($user['last_name']) ?></h3> 
	<div style="margin-left: 15px;">
	    <?= $this->Html->image('/uploads/'. h($user['profile_pic']), ['height' => '150px' , 'width' => '180px']) ?>
    </div>
    <div>
		<ul>
			<li><?= $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Followers'), 
                        ['controller' => 'users', 'action' => 'viewFollowers', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Following'), 
                        ['controller' => 'users', 'action' => 'viewFollowing', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Add post'), 
                        ['controller' => 'posts', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Edit Account'), 
                        ['controller' => 'users', 'action' => 'edit']); ?></li>
            <li><?= $this->Html->link(__('Logout'), 
                        ['controller' => 'users', 'action' => 'logout']); ?></li>
		</ul>
	</div>
</div>
<div class="users form" style='display: inline-block'>
    <fieldset>
        <legend><?php echo __('Add Post'); ?></legend>
            <?php
            echo $this->Form->create('Post', array('enctype' => 'multipart/form-data')); 
            echo $this->Form->input('title');
            echo $this->Form->input('message', ['rows' => '3']);
            ?> 
            <div style="width: 600px; border: 5px solid black; padding: 10px; margin: 30px;">
                <?php
                echo $this->Html->image('/uploads/'. h($post['User']['profile_pic']), [
                        'height' => '30px' , 'width' => '30px']);
                echo $this->Html->link(h($post['User']['username']), [
                        'controller' => 'users',
                        'action' => 'view', 
                        $post['User']['id']]);
                ?>
                <p><small>Created: <?php echo $post['Post']['created']; ?></small></p>
                <h1>
                    <?php 
                    echo $this->Html->link(h($post['Post']['title']), [
                        'controller' => 'posts',
                        'action' => 'view', 
                        $post['Post']['id']]);
                    ?>
                </h1>
                <p><?php echo h($post['Post']['message']); ?></p>
				<?php foreach ($post['Image'] as $image): ?>
                    <?php echo $this->Html->image('/uploads/'. h($image['filename']), 
                        ['height' => '100', 'width' => '100']); ?>
                <?php endforeach; ?>
            </div>
            <?php
            echo $this->Form->end('Share Post');
            ?>
    </fieldset>
</div>
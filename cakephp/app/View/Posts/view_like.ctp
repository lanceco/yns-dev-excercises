<div class='actions' style='display: inline-block'>
	<h3 style="text-align:center;"><?=  h($user['first_name']) ." ". h($user['last_name']) ?></h3> 
	<div style="margin-left: 15px;">
	    <?= $this->Html->image('/uploads/'. h($user['profile_pic']), ['height' => '150px' , 'width' => '180px']) ?>
    </div>
    <div>
		<ul>
			<li><?= $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Followers'), 
                        ['controller' => 'users', 'action' => 'viewFollowers', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Following'), 
                        ['controller' => 'users', 'action' => 'viewFollowing', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Add post'), 
                        ['controller' => 'posts', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Edit Account'), 
                        ['controller' => 'users', 'action' => 'edit']); ?></li>
            <li><?= $this->Html->link(__('Logout'), 
                        ['controller' => 'users', 'action' => 'logout']); ?></li>
		</ul>
	</div>
</div>
<div class="users form" style='display: inline-block'>
    <fieldset>
        <legend><?php echo __('Likes'); ?></legend>
        <table>
            <tr>
                <th>User</th>
            </tr>
            <?php foreach ($likes as $like): ?> 
            <tr>
                <td>
                    <?php 
                    echo $this->Html->link(h($like['User']['username']), [
                        'controller' => 'users',
                        'action' => 'view', 
                        $like['User']['id']]);
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php unset($post); ?>
        </table>
    </fieldset>
</div>


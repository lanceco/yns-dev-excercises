<div class='actions' style='display: inline-block'>
	<h3 style="text-align:center;"><?=  h($user['first_name']) ." ". h($user['last_name']) ?></h3> 
	<div style="margin-left: 15px;">
        <?= $this->Html->image('/uploads/'. h($user['profile_pic']), 
                ['height' => '150px' , 'width' => '180px']) ?>
    </div>
    <div>
		<ul>
			<li><?= $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Followers'), 
                        ['controller' => 'users', 'action' => 'viewFollowers', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Following'), 
                        ['controller' => 'users', 'action' => 'viewFollowing', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Add post'), 
                        ['controller' => 'posts', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Edit Account'), 
                        ['controller' => 'users', 'action' => 'edit']); ?></li>
            <li><?= $this->Html->link(__('Logout'), 
                        ['controller' => 'users', 'action' => 'logout']); ?></li>
		</ul>
	</div>
</div>

<div class="users form" style='display: inline-block'>
    <fieldset>
        <legend><?php echo __('Edit Post'); ?></legend>
            <?php
            echo $this->Form->create('Post', ['enctype' => 'multipart/form-data']);
            echo $this->Form->input('title');
            echo $this->Form->input('message', ['rows' => '3']);
            foreach ($post['Image'] as $image):
                echo '<div style="display: inline-block">';
                echo $this->Html->image('/uploads/'. h($image['filename']), ['height' => '100', 'width' => '100']);
                echo $this->Form->checkbox('edit_blog_pic', array('hiddenField' => false, 'value' => h($image['filename']),
                    'name' => 'data[Post][edit_blog_pic][]'));
                echo '</div>';
            endforeach;
            echo $this->Form->input('data[Post][edit_blog_pic][]', array('type' => 'hidden', 'value' => '0',
                'name' => 'data[Post][edit_blog_pic][]'));
            echo $this->Form->input('blog_pic', array('type' => 'file', 'multiple' => 'multiple',
                'name' => 'data[Post][blog_pic][]', 'label' => 'Post Pic'));
            
            // echo $this->Form->input('id', array('type' => '')); 
            echo $this->Form->end('Save Post');
            ?>
    </fieldset>
    <!-- <div class='actions'>
        <?= $this->Html->link('Cancel', ['controller' => 'posts', 'action' => 'index']) ?> 
    </div> -->
</div>
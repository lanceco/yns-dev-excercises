<div class='actions' style='display: inline-block'>
	<h3 style="text-align:center;"><?=  h($user['first_name']) ." ". h($user['last_name']) ?></h3> 
	<div style="margin-left: 15px;">
        <?= $this->Html->image('/uploads/'. h($user['profile_pic']), 
                ['height' => '150px' , 'width' => '180px']) ?>
    </div>
    <div>
		<ul>
			<li><?= $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Followers'), 
                        ['controller' => 'users', 'action' => 'viewFollowers', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Following'), 
                        ['controller' => 'users', 'action' => 'viewFollowing', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Add post'), 
                        ['controller' => 'posts', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Edit Account'), 
                        ['controller' => 'users', 'action' => 'edit']); ?></li>
            <li><?= $this->Html->link(__('Logout'), 
                        ['controller' => 'users', 'action' => 'logout']); ?></li>
		</ul>
	</div>
</div>
<div class="users form" style='display: inline-block'>
    <!-- <h1><?php echo h($post['Post']['title']); ?></h1> -->
    <?php 
    echo $this->Html->image('/uploads/'. h($post['User']['profile_pic']), ['height' => '30px' , 'width' => '30px']);
    echo $this->Html->link(h($post['User']['username']), [
            'controller' => 'users',
            'action' => 'view', 
            $post['User']['id']]);
    ?>
    <p><small>Created: <?php echo $post['Post']['created']; ?></small></p>
    <h1><?php echo h($post['Post']['title']); ?></h1>
    <p><?php echo h($post['Post']['message']); ?></p>
    
    <?php foreach ($post['Image'] as $image): ?>
        <?php echo $this->Html->image('/uploads/'. h($image['filename']), 
            ['height' => '100', 'width' => '100']); ?>
    <?php endforeach; ?>

    <?php if (count($sharePost) > 0): ?>
        <div style="width: 600px; border: 5px solid black; padding: 10px; margin: 30px;">
            <?php 
            echo $this->Html->image('/uploads/'. h($sharePost['User']['profile_pic']), [
                    'height' => '30px' , 'width' => '30px']);
            echo $this->Html->link(h($sharePost['User']['username']), [
                    'controller' => 'users',
                    'action' => 'view', 
                    $sharePost['User']['id']]);
            ?>
            <p><small>Created: <?php echo $sharePost['Post']['created']; ?></small></p>
            <h1>
                <?php 
                echo $this->Html->link(h($sharePost['Post']['title']), [
                    'controller' => 'posts',
                    'action' => 'view', 
                    $sharePost['Post']['id']]);
                ?>
            </h1>
            <p><?php echo h($sharePost['Post']['message']); ?></p>

            <?php foreach ($sharePost['Image'] as $image): ?>
                <?php echo $this->Html->image('/uploads/'. h($image['filename']), 
                    array('height' => '100', 'width' => '100')); ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <br>

    <div class='actions' style='display: inline-block; width:1000px;'>
        <?php
        // echo $this->Html->link(
        //     'Share',
        //     ['controller' => 'posts', 'action' => 'share', $post['Post']['id']],
        //     ['confirm' => 'Are you sure?']); 
        ?> 
        <?php
        echo $this->Html->link(
            'View Likes',
            ['controller' => 'posts', 'action' => 'viewLike', $post['Post']['id']]);
        ?>
        <?php
        echo $this->Html->link(
            'Like' .' ('. count($likes). ')',
            ['controller' => 'posts', 'action' => 'like', $post['Post']['id']]); 
        ?>  
    </div>
    <?php 
    echo $this->Form->create('Comment');
    echo $this->Form->input('message', ['rows' => '1', 'label' => 'Comment']);
    echo $this->Form->end('Post Comment');
    ?>
    <h1>Comments: </h1>
    <table>
        <tr>
            <th>Commented by</th>
            <th>Comment</th>
            <th>Action</th>
        </tr>
    <?php foreach ($post['Comment'] as $comment): ?>
        <tr>
            <td>
                <?php 
                echo $this->Html->link(h($comment['username']), [
                        'controller' => 'users',
                        'action' => 'view', 
                        $comment['user_id']]);
                ?>
            </td>
            <td><?php echo h($comment['message']) ?></td>
            <td class='actions'>
            <?php 
            if ($comment['user_id'] === AuthComponent::user('id')):
                echo $this->Form->postlink(
                    'Delete',
                    array('action' => 'deleteComment', $comment['id'], $comment['post_id']),
                    array('confirm' => 'Are you sure?')); 
                echo $this->Html->link(
                    'Edit',
                    array('action' => 'editComment', $comment['id'], $comment['post_id'])); 
            endif;
            ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>

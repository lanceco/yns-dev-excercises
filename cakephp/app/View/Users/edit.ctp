<div class='actions' style='display: inline-block'>
    <div>
		<ul>
			<li><?= $this->Html->link('Change Account', [
                        'controller' => 'users', 
                        'action' => 'changeAccount']) ?></li>
            <li><?= $this->Html->link('Change Profile ', [
                        'controller' => 'users', 
                        'action' => 'changeProfile']) ?></li>
            <li><?= $this->Html->link('Cancel', [
                        'controller' => 'posts', 
                        'action' => 'index']) ?></li>
		</ul>
	</div>
</div>
<div class="users form" style='display: inline-block'>
</div>
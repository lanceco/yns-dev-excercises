<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Reset Password'); ?></legend>
            <?php 
            echo $this->Form->input('password', ['label' => 'New password']);
            echo $this->Form->input('confirm_password', ['label' => 'Confirm new password', 'type' => 'password']);
            ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
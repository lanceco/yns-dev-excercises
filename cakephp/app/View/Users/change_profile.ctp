<div class='actions' style='display: inline-block'>
    <div>
		<ul>
			<li><?= $this->Html->link('Change Account', [
                        'controller' => 'users', 
                        'action' => 'changeAccount']) ?></li>
            <li><?= $this->Html->link('Change Profile ', [
                        'controller' => 'users', 
                        'action' => 'changeProfile']) ?></li>
            <li><?= $this->Html->link('Cancel', [
                        'controller' => 'posts', 
                        'action' => 'index']) ?></li>
		</ul>
	</div>
</div>
<div class="users form">
    <fieldset>
        <legend><?php echo __('Change Profile'); ?></legend>
            <?php 
            echo $this->Form->create('User', ['enctype' => 'multipart/form-data', 
                'onsubmit' => "return confirm('Submit?');"]); 
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->file('profile_pic');
            // echo $this->Form->input('profile_pic', ['type' => 'file']);
            echo $this->Form->end(__('Submit')); 
            ?>
    </fieldset>
</div>
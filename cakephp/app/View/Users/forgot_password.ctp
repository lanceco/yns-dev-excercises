<div class='actions' style='display: inline-block'>
    <div>
		<ul>
            <li><?= $this->Html->link('Register', [
                        'controller' => 'users', 
                        'action' => 'add']) ?></li>
            <li><?= $this->Html->link('Forgot Password', [
                        'controller' => 'users', 
                        'action' => 'forgotPassword']) ?></li>
			<li><?= $this->Html->link('Login', [
                        'controller' => 'users', 
                        'action' => 'login']) ?></li>
		</ul>
	</div>
</div>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Forgot Password'); ?></legend>
            <?php 
            echo $this->Form->input('email');
            ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
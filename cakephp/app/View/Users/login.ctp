<!-- app/View/Users/login.ctp -->
<div class='actions' style='display: inline-block'>
    <div>
		<ul>
			<li><?= $this->Html->link('Register', [
                        'controller' => 'users', 
                        'action' => 'add']) ?></li>
            <li><?= $this->Html->link('Forgot Password', [
                        'controller' => 'users', 
                        'action' => 'forgotPassword']) ?></li>
            <li><?= $this->Html->link('Login', [
                        'controller' => 'users', 
                        'action' => 'login']) ?></li>
		</ul>
	</div>
</div>
<div class='users form'>
    <?= $this->Flash->render('auth'); ?>
    <fieldset>
        <legend><?php echo __('Please enter your username and password'); ?></legend>
        <?php 
        echo $this->Form->create('User');
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->end(__('Login'));
        ?>
    </fieldset>
</div>
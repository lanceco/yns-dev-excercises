<div class='actions' style='display: inline-block'>
    <div>
		<ul>
            <li><?= $this->Html->link('Change Account', [
                        'controller' => 'users', 
                        'action' => 'changeAccount']) ?></li>
            <li><?= $this->Html->link('Change Profile ', [
                        'controller' => 'users', 
                        'action' => 'changeProfile']) ?></li>
            <li><?= $this->Html->link('Cancel', [
                        'controller' => 'posts', 
                        'action' => 'index']) ?></li>
		</ul>
	</div>
</div>
<div class="users form">
    <fieldset>
        <legend><?php echo __('Change Account'); ?></legend>
            <?php 
            echo $this->Form->create('User', 
                ['onsubmit' => "return confirm('Submit?');"]); 
            echo $this->Form->input('username');
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('confirm_password', ['type'  =>  'password']); 
            echo $this->Form->end(__('Submit'));
            ?>
    </fieldset>
</div>

<!-- File: /app/View/Posts/index.ctp -->
<!-- <?php $currentUserId = AuthComponent::user('id'); ?> -->
<div class='actions' style='display: inline-block'>
	<h3 style="text-align:center;"><?= h($user['User']['first_name']) ." ". h($user['User']['last_name']) ?></h3> 
	<div style="margin-left: 15px;">
        <?= $this->Html->image('/uploads/'. h($user['User']['profile_pic']), 
                ['height' => '150px' , 'width' => '180px']) ?>
    </div>
    <div>
		<ul>
			<li><?= $this->Html->link(__('Home'), ['controller' => 'posts', 'action' => 'index']); ?></li>
            <?php if (Authcomponent::user('id') === $user['User']['id']): ?>
            <li><?= $this->Html->link(__('Followers'), 
                        ['controller' => 'users', 'action' => 'viewFollowers', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Following'), 
                        ['controller' => 'users', 'action' => 'viewFollowing', $user['id']]); ?></li>
            <li><?= $this->Html->link(__('Add post'), 
                        ['controller' => 'posts', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Edit Account'), 
                        ['controller' => 'users', 'action' => 'edit']); ?></li>
            <?php else: ?>
            <li><?= $this->Html->link(__('Follow'), [
                                'controller' => 'users', 
                                'action' => 'follow', $user['User']['id']]);  ?></li>
            <?php endif; ?>
            <li><?= $this->Html->link(__('Logout'), 
                        ['controller' => 'users', 'action' => 'logout']); ?></li>
		</ul>
	</div>
</div>
<div class="users form" style='display: inline-block'>
    <table>
        <tr>
            <th>Title</th>
            <th>Actions</th>
            <th>Time</th>
        </tr>
        <?php foreach ($user['Post'] as $post): ?>
        <tr>
            <td>
                <?php 
                echo $this->Html->link(
                    h($post['title']),
                    ['controller' => 'posts','action' => 'view', 
                    $post['id']]
                ); 
                ?>
            </td>
            <td class='actions'>
                <?php
                if ($currentUser['User']['id'] === $user['User']['id']): 
                    echo $this->Form->postlink(
                        'Delete',
                        ['controller' => 'posts', 'action' => 'delete', $post['id']],
                        ['confirm' => 'Are you sure?']
                    ); 
                    echo $this->Html->link(
                        'Edit',
                        ['controller' => 'posts', 'action' => 'edit', $post['id']]
                    );
                endif; 
                echo $this->Html->link(
                    'Share',
                    ['controller' => 'posts', 'action' => 'share', $post['id']],
                    ['confirm' => 'Are you sure?']
                ); 
                ?>
            </td>
            <td><?php echo $post['created']; ?></td>
        </tr>
        <?php endforeach; ?>
        <?php unset($post); ?>
    </table>
</div>
    










<!-- <h1><?php echo h($user['User']['username']); ?></h1>
<p><?php echo h($user['User']['first_name']); ?></p>
<p><?php echo h($user['User']['last_name']); ?></p>
<h1>Posts:</h1>
<?php
foreach($user['Post'] as $post){
    echo h($post['title']); 
    echo h($post['message']); 
    echo '<br>';
}
?> -->
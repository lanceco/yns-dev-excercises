<div class='actions' style='display: inline-block'>
    <div>
		<ul>
            <li><?= $this->Html->link('Register', [
                        'controller' => 'users', 
                        'action' => 'add']) ?></li>
            <li><?= $this->Html->link('Forgot Password', [
                        'controller' => 'users', 
                        'action' => 'forgotPassword']) ?></li>
			<li><?= $this->Html->link('Login', [
                        'controller' => 'users', 
                        'action' => 'login']) ?></li>
		</ul>
	</div>
</div>
<div class="users form">
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
            <?php 
            echo $this->Form->create('User');
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('confirm_password',array('type'  =>  'password'));
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name'); 
            // echo $this->Form->input('role', 
            //     array('options' => 
            //         array('admin' => 'Admin', 'author' => 'Author')));
            echo $this->Form->input('email');
            echo $this->Form->end(__('Submit'));
            ?>
    </fieldset>
</div>
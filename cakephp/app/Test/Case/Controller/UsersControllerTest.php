<?php
App::uses('UsersController', 'Controller');

/**
 * UsersController Test Case
 */
class UsersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	// public $fixtures = array(
	// 	'app.user',
	// 	'app.post',
	// 	'app.like',
	// 	'app.comment',
	// 	'app.image',
	// 	'app.follow'
	// );

/**
 * testApiLogin method
 *
 * @return void
 */
	public function testApiLogin() {
		$this->assertEquals(1, 1);
	}

/**
 * testLogin method
 *
 * @return void
 */
	public function testLogin() {
		$this->markTestIncomplete('testLogin not implemented.');
	}

/**
 * testLogout method
 *
 * @return void
 */
	public function testLogout() {
		$this->markTestIncomplete('testLogout not implemented.');
	}

/**
 * testFollow method
 *
 * @return void
 */
	public function testFollow() {
		$this->markTestIncomplete('testFollow not implemented.');
	}

/**
 * testViewFollowers method
 *
 * @return void
 */
	public function testViewFollowers() {
		$this->markTestIncomplete('testViewFollowers not implemented.');
	}

/**
 * testViewFollowing method
 *
 * @return void
 */
	public function testViewFollowing() {
		$this->markTestIncomplete('testViewFollowing not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testForgotPassword method
 *
 * @return void
 */
	public function testForgotPassword() {
		$this->markTestIncomplete('testForgotPassword not implemented.');
	}

/**
 * testResetPassword method
 *
 * @return void
 */
	public function testResetPassword() {
		$this->markTestIncomplete('testResetPassword not implemented.');
	}

/**
 * testActivation method
 *
 * @return void
 */
	public function testActivation() {
		$this->markTestIncomplete('testActivation not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testChangeAccount method
 *
 * @return void
 */
	public function testChangeAccount() {
		$this->markTestIncomplete('testChangeAccount not implemented.');
	}

/**
 * testChangeProfile method
 *
 * @return void
 */
	public function testChangeProfile() {
		$this->markTestIncomplete('testChangeProfile not implemented.');
	}

}

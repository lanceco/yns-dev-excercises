<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>5-2</title>
    </head>
    <body>
        <div align="center">
            <h3 id="monthAndYear"></h3>
            <table>
                <thead>
                    <tr>
                        <th>Sun</th>
                        <th>Mon</th>
                        <th>Tue</th>
                        <th>Wed</th>
                        <th>Thu</th>
                        <th>Fri</th>
                        <th>Sat</th>
                    </tr>
                </thead>

                <tbody id="calendar-body">
                </tbody>
            </table>
        </div>
        <div align="center">
            <button id="previous" onclick="previous()">Previous</button>
            <button id="next" onclick="next()">Next</button>
        </div>
        <p id="demo"></p>
        <script src="5-2.js"></script>
    </body>
</html>

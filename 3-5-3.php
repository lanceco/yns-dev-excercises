<!DOCTYPE html>
<?php 
	session_start();
	if(!isset($_SESSION['username'])) {
		header('location:3-5.php');
	}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>3-5-3</title>
		<script>
			function myFunction(x) {
				if (x == 'goback') window.location.href = "3-5-2.php";
				if (x == 'records') window.location.href = "3-5-4.php";
			}
		</script>
	</head>
	<body>
		<?php 
			$servername = "localhost";
			$username = "root";
			$password = "";
			$database = "test";
			$first_name = $_POST['fName'];
			$last_name = $_POST['lName'];
			$email = $_POST['email'];
			$age = $_POST['age'];
			$gender = $_POST['gender'];
			$fileToUpload = "";
			$error = false;
		?>
		<table align="center">
		    <tr>
		        <td>First name: </td>
		        <td><?php 
			        	if (empty($first_name)) {
			        		echo '<span style="color:red">First name is required</span>';
			        		$error = true;	
			        	} else if (is_numeric($first_name)) {
		        			echo '<span style="color:red">First name must have characters</span>';
		        			$error = true;
			        	} else {
			        		echo $first_name;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Last name:</td>
		        <td><?php 
			        	if (empty($last_name)) {
			        		echo '<span style="color:red">Last name is required</span>';
			        		$error = true;	
			        	} else if (is_numeric($last_name)) {
		        			echo '<span style="color:red">Last name must have characters</span>';
		        			$error = true;
			        	} else {
			        		echo $last_name;
			        	}
		        	?>		
		        </td>
		    </tr>

			<tr>
		        <td>Email address:</td>
		        <td><?php 
			        	if (empty($email)) {
			        		echo '<span style="color:red">Email is required</span>';
			        		$error = true;	
			        	} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		        			echo '<span style="color:red">Invalid email format</span>';
		        			$error = true;
			        	} else {
			        		echo $email;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Age:</td>
		        <td><?php 
			        	if (empty($age)) {
			        		echo '<span style="color:red">Age is required</span>';
			        		$error = true;	
			        	} else if (!is_numeric($age)) {
		        			echo '<span style="color:red">Age must be numeric</span>';
		        			$error = true;
			        	} else {
			        		echo $age;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Gender:</td>
		        <td><?php 
			        	if (empty($gender)) {
			        		echo '<span style="color:red">Gender is required</span>';
			        		$error = true;	
			        	} else {
			        		echo $gender;
			        	}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		        <td>Image:</td>
		        <td><?php
		        		if($_FILES["fileToUpload"]["error"] == 4) {
							//means there is no file uploaded
		        			echo '<span style="color:red">No image was uploaded.</span><br>';
		        			$error = true;
						} else {
				        	$target_dir = "uploads/";
							$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
							$uploadOk = 1;
							$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
							// Check if image file is a actual image or fake image
							if(isset($_POST["submit"])) {
							    $check =	 getimagesize($_FILES["fileToUpload"]["tmp_name"]);
							    if($check !== false) {
							        // echo "File is an image - " . $check["mime"] . ".";
							        $uploadOk = 1;
							    } else {
							        echo '<span style="color:red">File is not an image.</span><br>';
							        $uploadOk = 0;
							        $error = true;
							    }
							}

							// Check if file already exists
							// if (file_exists($target_file)) {
							//     echo '<span style="color:red">Sorry, file already exists.</span><br>';
							//     $uploadOk = 0;
							//     $error = true;
							// }

							// Check file size
							if ($_FILES["fileToUpload"]["size"] > 500000) {
							    echo '<span style="color:red">Sorry, your file is too large.</span><br>';
							    $uploadOk = 0;
							    $error = true;
							}

							// Allow certain file formats
							if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
							&& $imageFileType != "gif" ) {
							    echo '<span style="color:red">Sorry, only JPG, JPEG, PNG & GIF files are allowed.</span><br>';
							    $uploadOk = 0;
							    $error = true;
							}
							if (!$error) {
								// Check if $uploadOk is set to 0 by an error
								if ($uploadOk == 0) {
								    echo '<span style="color:red">Sorry, your file was not uploaded.</span><br>';
								    $error = true;
								// if everything is ok, try to upload file
								} else {
								    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
								        $fileToUpload = basename($_FILES["fileToUpload"]["name"]);
								        echo '<img src= "uploads/'. $fileToUpload .'" alt="Profile Image" height="50" width="50">';
								        echo "The file ". $fileToUpload. " has been uploaded.";

								    } else {
								         echo '<span style= "color:red">Sorry, there was an error uploading your file.</span><br>';
								         $error = true;
								    }
								}
							} else {
								echo '<span style="color:red">Sorry, your file was not uploaded.</span><br>';
							}
						}
		        	?>		
		        </td>
		    </tr>

		    <tr>
		    	<?php
		    		if (!$error) {
		    			//Create connection
						$conn = mysqli_connect($servername, $username, $password, $database);
						//Check connection
						if (!$conn) die("Connection Failed: ". mysqli_connect_error());
						// echo "Successfully Connected"; 

						//Prepare and bind
						$stmt = $conn->prepare("INSERT INTO persons (firstname, lastname, email, age, gender, image) VALUES (?,?,?,?,?,?)");
						$stmt->bind_param("sssiss", $first_name, $last_name, $email, $age, $gender, $fileToUpload);

						$stmt->execute();
						echo "<td><span style='color:green'>New record added successfully.</span></td>";
						$stmt->close();
						$conn->close();
		    		} 
		    	?>
		    </tr>

		    <tr>
		    	<td><button onclick="myFunction('records')">Records</button></td>
		    </tr>
		    <tr>
		    	<td><button onclick="myFunction('goback')">Go Back</button></td>
		    </tr>	
		</table>
	</body>
</html>
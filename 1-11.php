<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-11</title>
	</head>
	<body>
		<table align="center">
		    <tr>
		        <td>First name</td>
		        <td>Last name</td>
		        <td>Email</td>
		        <td>Age</td>
	        	<td>Gender</td>
	        	<td>Images</td>
		    </tr>
	        <?php
	        	$fp = fopen("person.csv", "r");
	        	$i = 0;
	        	$numImages = count(fgetcsv($fp));
			    while (($line = fgetcsv($fp)) !== FALSE) {
			        echo '<tr>';
			        foreach ($line as $cell) {
			            if (++$i === $numImages) {
	            			echo '<td><img src="uploads/'. htmlentities($cell) .'" alt="Profile Image" height="50" width="50"></tr>';
			            	$i = 0;
			            } else {
				            echo '<td>'. htmlentities($cell) .'</td>';
			            } 
			        }
			        echo '</tr>';
			    }
			    fclose($fp);
			?>
		</table>
	</body>
</html>

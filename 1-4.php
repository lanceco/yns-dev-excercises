<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>1-4</title>
	</head>
	<body>
		<?php 
			$result = "";
			class fizzBuzz {
			    var $a;

			    function getresult($a) {
			    	$output = "";
			    	for ($i = $a; $i > 0; $i--) {
		    			if ($i % 3 == 0 && $i % 5 == 0) {
		    				$output .= " FizzBuzz ";	
		    			} else if ($i % 3 == 0) {
		    				$output .= " Fizz ";
	    				} else if ($i % 5 == 0) {
	    					$output .= " Buzz ";
	    				} else {
	    					$output .= " " .$i. " ";
	    				}
			    	}

			    	return $output;
			    }
			}

			$fizzBuzz = new fizzBuzz();

			if(isset($_POST['submit'])) {   
			    $result = $fizzBuzz->getresult($_POST['n1']);
			}
		?>

		<form method="post">
			<table align="center">
			    <tr>
			        <td>Enter a Number</td>
			        <td><input type="number" name="n1"></td>
			    </tr>

			    <tr>
			        <td></td>
			        <td><input type="submit" name="submit" value="                =                "></td>
			    </tr>
			</table>
			<strong><?php echo $result; ?><strong>
		</form>
	</body>
</html>
